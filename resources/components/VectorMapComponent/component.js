/*

 Regions are coded using ISO 3166.

 Implemented as a wrapper over JQuery-Mapael

 */



var VectorMapComponent = (function (){

  var _mapDefaults = {},
      _mapDefaultsJVM={
    regionsSelectable: true,
    backgroundColor: "#FFFFFF",
    regionStyle: {
      initial: { "fill": '#E5E4E4' },
      hover: { "fill-opacity": 0.8 }
    },
    onRegionOver: function (event, code, region) {
      document.body.style.cursor = "pointer";
    },
    onRegionOut: function (event, code, region) {
      document.body.style.cursor = "default";
    },
    onMarkerOver: function (event, code, region) {
      document.body.style.cursor = "pointer";
    },
    onMarkerOut: function (event, code, region) {
      document.body.style.cursor = "default";
    }
  },
      _defaults = {
        mapId: 'world_mill_en',
        isoFormat: true,
        minColor: "#E5E4E4",
        maxColor: "#EF720B"
      };


  var MyClass = UnmanagedComponent.extend({
    _loadInParallel: true,
    get: function(opt){
      var cd = this.chartDefinition;
      return !_.isUndefined(cd[opt]) ? cd[opt] :
        ( !_.isUndefined( _defaults[opt]) ? _defaults[opt] : undefined );
    },
    _isMapLoaded: function(mapId){
      if (!mapId){
        mapId = this.get('mapId');
      }
      return (mapId in jQuery.fn.mapael.maps);
    },

    devSandbox: function(){
      var f = function(canvas, marker, markerStyle, markersGroup){
        Dashboards.log('Rendering marker');
        switch(2){
        case 1:
          var el = new jvm[canvas.classPrefix + 'CircleElement'](marker, markerStyle);
          canvas.add( el, markersGroup);
          return el;
        case 2:
          var f = Snap.fragment('<?xml version="1.0" encoding="UTF-8" standalone="no"?> <!-- Created with Inkscape (http://www.inkscape.org/) --> <svg xmlns:dc="http://purl.org/dc/elements/1.1/"xmlns:cc="http://creativecommons.org/ns#"xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"xmlns:svg="http://www.w3.org/2000/svg"xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"version="1.1"width="744.09448"height="1052.3622"id="svg2"> <defs id="defs4"> <linearGradient id="linearGradient3787"> <stop id="stop3789"style="stop-color:#505050;stop-opacity:1"offset="0" /> <stop id="stop3791"style="stop-color:#505050;stop-opacity:0"offset="1" /> </linearGradient> <filter x="-0.30034521"y="-0.097170517"width="1.6006904"height="1.1943409"color-interpolation-filters="sRGB"id="filter3783"> <feGaussianBlur id="feGaussianBlur3785"stdDeviation="11.799276" /> </filter> <linearGradient x1="168.3246"y1="380.93359"x2="254.53255"y2="383.79074"id="linearGradient3793"xlink:href="#linearGradient3787"gradientUnits="userSpaceOnUse" /> </defs> <metadata id="metadata7"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g id="layer1"> <rect width="94.285713"height="291.42856"x="131.42857"y="238.07646"id="rect3001"style="opacity:0.94420603;fill:#50a050;fill-opacity:0.50196078;stroke:url(#linearGradient3793);stroke-linecap:round;stroke-opacity:0.50196078;" /> <text x="128.57143"y="243.79076"id="text3797"xml:space="preserve"style="font-size:40px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"><tspan x="128.57143"y="243.79076"id="tspan3799">28 %</tspan></text> </g> </svg>');
          var p = Snap(); //Snap(canvas.container);
          p.attr({
            'data-index': marker['data-index']
          });
          //f.select('svg').attr({height: 100});
          p.append(f.select('svg g'));
          // var r = p.rect(marker.cx, marker.cy, 100, 100, 3, 3).attr({
          //   'fill':'white',
          //   'fill-opacity': 0,
          //   stroke: '#000',
          //   'stroke-width':1
          // });
          var c = p.circle(marker.cx, marker.cy, 10);
          p.addClass = function( className ){
            this.node.setAttribute('class', className);
          };
          p.setStyle = function( style ){
            if (!_.isObject(style)){
              var o = {};
              o[arguments[0]] = arguments[1];
              style = o;
            }
            var keys = {
              cx: 'x',
              cy: 'y',
              r: 'height',
              fill: 'fill',
              stroke: 'stroke'
            };
            c.attr(style);
            _.each(style, function(value, key){
              if (key in keys){
                var opt = {};
                opt[keys[key]] = value;
                p.select('*').attr(opt);
              }
              switch (key){
              case 'r':
                p.select('g text tspan').node.innerHTML = value + "%";
                p.select('g rect').attr({height: value*10});
                break;
              case 'cx':
                p.select('#rect3001').attr({x: value});
                p.select('g text tspan').attr({x: value});
                break;
              case 'cy':
                p.select('#rect3001').attr({y: value});
                p.select('#tspan3799').attr({y: value});
                break;
              case 'stroke':
                p.select('g text tspan').attr({fill: value});
                break;
              // default:
              //   var o = {};
              //   if (key){
              //     o[key] = value;
              //     p.select('g text span').attr(o);
              //   }

              }
            });
          };
          p.setHovered = function(bool){
            var style = bool ? {
              stroke: 'blue',
              'stroke-width': 2
            } : {
              stroke: 'black',
              'stroke-width': 1
            };
            //style = bool ? markerStyle.hover : style.initial;
            // r.attr(style);
            c.attr(style);
            p.select('g text tspan').attr(style);
          };
          canvas.add(p, markersGroup);
          return p;
        case 3:
          var el2 = new jvm[canvas.classPrefix + 'ShapeElement']('rect', {x:marker.cx, y:marker.cy, width:30, height:50}, {fill:'red', stroke:'blue'});
          canvas.add(el2, markersGroup);
          return el2;
        }
      };
      var g = function(canvas, marker, style, markersGroup){
        Dashboards.log('Rendering another marker using the original style');
        return canvas.addCircle(marker, style, markersGroup);
      };
      this.markerDefinitions = {
        "AO-LUA":{ latLng : [-8.8169602,13.2381607], name: 'Luanda', renderer: f },
        "AO-MOX":{ latLng:  [-12.7287195,21.2737637], name: 'Luena', renderer: g },
        "AO-NAM":{ latLng: [-15.19611, 12.15222], name: 'Namibe'},
        "AO-ZAI":{ latLng: [-6.135, 12.369], name: 'Soyo'}
      };
      //jvm.WorldMap.maps = {}; //DEBUG: remove all preloaded maps
    },

    update: function() {
      //this.devSandbox();
      this.preExec(); // Hack to detect map
      var ph = this.placeholder().empty(),
          croppedCd = $.extend({}, this.chartDefinition, { drawCallback:undefined }),
          callback = _.bind(this.render,this);

      this.$container = $("<div />").addClass('vectormap-container').appendTo(ph);

      if ( this._isMapLoaded()){
        this.triggerQuery( croppedCd, callback );
      } else {
        // Load a map asynchronously.
        // TODO: consider using $.when() to paralellize triggerQuery and $.getScript
        var url,
            myself = this;
        if (this.chartDefinition.mapLocation.endsWith('.js')) {
          url = this.chartDefinition.mapLocation; // allows hacking by specifying the file directly
          this.chartDefinition.mapId = url.split('jquery-jvectormap-')[1].split('.js')[0].split('-').join('_');
        } else {
          url = this.chartDefinition.mapLocation + '/jquery-jvectormap-' + this.chartDefinition.mapId.split('_').join('-') + '.js';
        }
        if (this._loadInParallel){
          $.getScript(url).then( function(){
            // this will run preExecution again... a minor price to pay
            myself.triggerQuery( croppedCd, callback );
          });
        } else if (false) {
          // Launch queries in parallel
          var launchQuery = function(){
            var deferred = $.Deferred();
            myself.triggerQuery( croppedCd, function(data){
              if (data){
                deferred.resolve(data);
              } else {
                deferred.reject();
              }
            });
            return deferred.promise();
          };

          $.when( launchQuery(), $.getScript(url) ).done( function(data){
            callback(data);
          });
        } else {
          //note that preExec was already run
          this.deferredTriggerQuery( croppedCd, [ $.getScript(url) ], callback);
        }
      }
    },

    deferredTriggerQuery: function (queryDef, deferreds, callback, userQueryOptions) {
      /*
       * Variation of the triggerQuery, supporting deferreds
       * The triggerQuery lifecycle handler builds a lifecycle around Query objects.
       *
       * It takes a query definition object that is passed directly into the Query
       * constructor, and the component rendering callback, and implements the full
       * preExecution->block->render->postExecution->unblock lifecycle. This method
       * detects concurrent updates to the component and ensures that only one
       * redraw is performed.
       */

      var silent = this.isSilent();
      if (!silent){
        this.block();
      };
      userQueryOptions = userQueryOptions || {};
      /*
       * The query response handler should trigger the component-provided callback
       * and the postExec stage if the call wasn't skipped, and should always
       * unblock the UI
       */
      var success = _.bind(function(data) {
        // callback(data);
        //this.postExec();
      }, this);
      var always = _.bind(function (){
        if (!silent){
          this.unblock();
        }
      }, this);
      var handler = this.getSuccessHandler(success, always),
          errorHandler = this.getErrorHandler();

      var query = this.queryState = this.query = Dashboards.getQuery( queryDef );
      var ajaxOptions = {
        async: true
      };
      if(userQueryOptions.ajax) {
        _.extend(ajaxOptions,userQueryOptions.ajax);
      }
      query.setAjaxOptions(ajaxOptions);
      if(userQueryOptions.pageSize){
        query.setPageSize(userQueryOptions.pageSize);
      }


      // Setup a function that launches a query and returns a deferred object
      // Each deferred object is effectively a task that runs in parallel
      var myself = this;
      var launchQuery = function(){
        var _deferredQuery = $.Deferred();
        query.fetchData(myself.parameters, function(){
          // handler.apply(myself, arguments);
          _deferredQuery.resolve.apply(myself, arguments);
        }, function(){
          errorHandler.apply(myself, arguments);
          _deferredQuery.reject();
        });
        return _deferredQuery.promise().done(always); // danger: is this done or always?
      };

      var deferredSuccess = function(datasets){
        if (_.isFunction(myself.postFetch)){
          datasets = myself.postFetch(datasets);
        }
        if ( _.isFunction(callback) && (datasets !== false) ){
          callback(datasets);
        }
        myself.postExec();
      };

      // Launch all tasks, run postExec once all are successful
      deferreds = deferreds || [];
      deferreds.unshift( launchQuery() ); //prepend
      return $.when.apply(null, deferreds).then( deferredSuccess );
    },

    _scaleSignificantDigits:2, // Round values displayed on the color scale to N significant digits
    setRange: function(values){
      //var cd = this.chartDefinition;
      this._min = !_.isNumber(this.get('min')) ? jvm.min(values) : this.get('min');
      this._max = !_.isNumber(this.get('max')) ? jvm.max(values) : this.get('max');



      if (this._scaleSignificantDigits !== undefined){
        //                // Hack to allow the scale to be rounded
        //                var roundScale = function (n, p) {
        //                    // Round to lower significant digit
        //                    var m =Math.pow(10, (n.toString().match(/[0-9]+/)[0].length-Math.abs(p) ));
        //                    return Math.round(n/m + p)*m;
        //                };

        var roundScale = function (n, sig) {
          var mult = Math.pow(10, Math.abs(sig) - Math.floor(Math.log(n) / Math.LN10) - 1);
          if (_.isNaN(mult)){
            mult = 1;
          }
          var val = function(p) {return Math.round(Math.round(n * mult + p) / mult);};
          if (val(0) == n){
            return n;
          } else {
            return val(0.5) || 0;
          }
        };
        this._min = roundScale(this._min, -this._scaleSignificantDigits);
        this._max = roundScale(this._max, this._scaleSignificantDigits);
      }

      // MTinsight bugfixes:
      if (_.isNaN(this._max)){
        this._max = 0;
      }
      this._min = 0;
      // End MTInsight bugfixes


    },

    _scaleSteps: 10, // Number of steps to display on the color scale
    drawScale: function( values) {
      var steps = this._scaleSteps,
          color, i;

      var minValue, maxValue;
      var minColor = this.get('minColor'),
          maxColor = this.get('maxColor');

      var ft = this.get('valueFormat');
      if( _.isFunction( ft ) ) {
        minValue = ft(this._min);
        maxValue = ft(this._max);
      } else {
        minValue = sprintf('%d', this._min);
        maxValue = sprintf('%d', this._max);
      }

      this.$scale = $("<div />").addClass('jqvm-scale').appendTo(this.$container);
      this.$scale.append('<div class="min-value">'+minValue+'</div>');
      for(i = 0; i < steps; i++) {
        color = this.interpolateColor( minColor, maxColor, steps, i+1 ); //Choose the color in between
        //this.$scale.append('<div class="scale-item" style="background-color:'+color+'">&nbsp;</div>');
        var nVal  = this._min + (this._max-this._min) * (i/(steps));
        var nVal2 = this._min + (this._max-this._min) * ((i+1)/(steps));
        var title = sprintf('%d - %d',nVal, nVal2);
        this.$scale.append('<div class="scale-item" style="background-color:'+color+';" title="'+ title+'">&nbsp;</div>');

      }
      this.$scale.append('<div class="max-value">'+maxValue+'</div>');
    },


    render: function(data) {
      var myself = this;
      this.mapData = jQuery.fn.mapael.maps[this.get('mapId')];

      var values = this.prepareData(data.resultset);
      //this.setRange(values);

      var w = this.get('width'), h = this.get('height');
      w = ( _.isNumber( w ) && w+'px' )  ||  ( _.isEmpty(w) && '100%' )  ||
        ( _.isString( w ) && w )       ||  '100%';
      h = ( _.isNumber( h ) && h+'px' )  ||  ( _.isEmpty(h) && '100%' )  ||
        ( _.isString( h ) && h )       ||  '100%';
      this.$container.css({ x:0, y:0, width:w , height: h });

      this.$container.empty()
        .append($('<div />').addClass('map'))
        .append($('<div />').addClass('areaLegend'));

      // Workaround for a bug in JVectorMap: it seems not to respect an externally supplied series.min
      // when we have data only on a single key. Adding a fake key set to this._min seems tot solve it.
      //values['nothing.min'] = this._min;
      //values['nothing.max'] = this._max;


      this.$container.mapael( this.buildMapModel(values) );
      //this.chart = this.$container.vectorMap( 'get' , 'mapObject');
      //this.drawScale(values);
      //this.setupCallbacks();

	    // Zoom on mousewheel with mousewheel jQuery plugin
	    this.$container.on("mousewheel", function(e) {
		    if (e.deltaY > 0){
			    myself.$container.trigger("zoom", myself.$container.data("zoomLevel") + 1);
        } else {
			    myself.$container.trigger("zoom", myself.$container.data("zoomLevel") - 1);
        }

		    return false;
	    });
    },

    setupCallbacks: function(){
      var myself = this;

      this.on('regionClick', function(event, code){
        Dashboards.log('Click on region ' + code);
        event.preventDefault();
      });

      this.onRegionClick =  function(event, code){
        Dashboards.log('Another Click on region ' + code);
        event.preventDefault();
      };


      // Add callbacks
      var callbacks = {
        markerLabelShow: 'markerLabelShow.jvectormap',
        markerOver: 'markerOver.jvectormap',
        markerOut: 'markerOut.jvectormap',
        markerClick: 'markerClick.jvectormap',
        regionLabelShow: 'labelShow.jvectormap',
        regionOver: 'regionOver.jvectormap',
        regionOut: 'regionOut.jvectormap',
        regionClick: 'regionClick.jvectormap'
      };
      _.each(callbacks, function(jvmCallback, key){
        myself.$container.bind(jvmCallback, function(){
          myself.trigger.apply(myself, _.union([key], arguments)); //hook jvectormap events to Backbone
        });
        var exposedCallback = 'on'+ key[0].toUpperCase() + key.slice(1);
        Dashboards.log('Hooking up callback ' + exposedCallback);
        if (_.isFunction(myself[exposedCallback])) {
          myself.on(key, myself[exposedCallback]);
        }
      });



    },

    buildMapModel: function(values){
      var myself =  this,
          opts = {}; //$.extend(true, {}, this.getMapDefaults());

      var minColor = tinycolor(this.get('minColor')).toHexString(),
          maxColor = tinycolor(this.get('maxColor')).toHexString();


      opts.map = {
        name: this.get('mapId'),
        zoom: {
          enabled: true,
          maxLevel: 10
        }
      };

      opts.defaultArea = {
        attrs: {
          animDuration: 0
        },
        attrsHover: {
          animDuration: 0
        }
      };
      opts.defaultPlot = {
        attrsHover: {
          animDuration: 0
        }
      };

      var regions = _.map(this.mapData.areas, function(val, key){
        return {
          value: myself.allSeries[key][0],
          scale: [_.min(values), _.max(values)],
          colors: [minColor, maxColor]
        };
      });
     // var allValues = _.map(myself.allSeries, function(v){ return v[0];});
      var relDepth = (_.max(values) - _.min(values) );
      opts.areas = $.extend(true, {}, this.mapData.areas);
      _.each(opts.areas, function (val, key){
        opts.areas[key] = {
          value: myself.allSeries[key][0],
          scale: [_.min(values), _.max(values)],
          colors: [minColor, maxColor],
          attrs:{
            //fill:  myself.interpolateColor(minColor,maxColor,relDepth,myself.allSeries[key][0] - _.min(values))
            fill:  myself.interpolateColor(minColor,maxColor,1, Math.random()),
            cursor: 'pointer'
          },
          attrsHover: {
            animDuration: 0,
            stroke: 'blue'
          }
        };
      });


      opts.legend ={
        area : {
          display : false,
          slices: []
        }
      };
      var range = _.range(_.min(values), _.max(values), 256);
      opts.legend.area.slices = _.map(range, function(val, idx){
        return {
          max: val,
          min: range[idx -1],
          fill:  myself.interpolateColor(minColor,maxColor,1, Math.random())
        };
      });

      this._opts = opts;
      return opts;
      //
      opts.markers = this.markerDefinitions;

      opts.markerStyle = {
        initial: {
          fill: 'grey',
          stroke: '#505050',
          "fill-opacity": 1,
          "stroke-width": 1,
          "stroke-opacity": 1,
          r: 5
        },
        hover: {
          stroke: 'black',
          "stroke-width": 2,
          fill: 'blue'
        },
        selected: {
          fill: 'blue'
        },
        selectedHover: {
        }
      };
      opts.map = this.get('mapId');
      opts.series = {
        regions:[
          {
            attribute: "fill",
            scale: [ minColor, maxColor ],
            values: values,
            min: this._min,
            max: this._max
          }
        ],
        markers:[
          {
            attribute: "fill",
            scale: [ minColor, maxColor ],
            values: values,
            min: this._min,
            max: this._max
          },
          {
            attribute: "r",
            scale: [ 5, 20 ],
            values: values,
            min: this._min,
            max: this._max
          }
          // {
          //   attribute: "customMarkerRenderer",
          //   renderer: this.onMarkerDraw,
          //   values: values,
          //   min: this._min,
          //   max: this._max
          // }
        ]
      };

      opts.onRegionSelected = function(ev, code, isSelected, selection) {
        myself.lastValues = (myself.get('isoFormat').toLowerCase() == "true") ? selection : selection.map(function(v){
          return myself.mapData.paths[v].name;
        });
        Dashboards.processChange(myself.name); //WTF? why should we invoke a change here?
      };
      opts.onMarkerSelected = function(ev, code, isSelected, selection) {
        myself.lastValues = (myself.get('isoFormat').toLowerCase() == "true") ? selection : selection.map(function(v){
          return myself.mapData.paths[v].name;
        });
        Dashboards.processChange(myself.name); //WTF? why should we invoke a change here?
      };


      $.extend(opts, myself.get('extraOpts'));
      return opts;
    },

    getValue: function() {
      return this.lastValues;
    },

    setValues: function (values){
      var newValues = this.prepareData(values);
      this._regionValues = newValues;
      //this.chart.series.regions[0].setValues( newValues );
    },

    prepareData: function(values) {
      /** Validates regions

       if not isoFormat, map "Alabama" -> "US-AL"
       */


      var mapKeys = {},
          data = {},
          myself = this;
      _.each( this.mapData.areas, function(el, key){
        mapKeys[ (myself.get('isoFormat').toLowerCase() == "true") ? key : el.name] = key;
        //data[key] = Math.random()*1000;
      });

      myself.allSeries = {}; // this will hold all columns except for the first two (key, value)
      _.each( values, function(row){
        var key = row[0];
        if( key in mapKeys) {
          data[mapKeys[key]] = row[1];
          myself.allSeries[mapKeys[key]] = [];
          _.each(row.slice(1), function(el){
            myself.allSeries[mapKeys[key]].push(el);
          });
        }
      });

      return data;
    },

    setSize: function (lag, steps){
      steps = ( _.isNumber(steps) && (steps>=1) && steps) || 20;
      lag =  _.isNumber(lag) && (lag>0) && lag;
      var stepSize = lag / steps,
          myself = this;

      clearInterval( this._setIntervalHandler);
      myself.chart.setSize();

      this._setIntervalHandler = setInterval( function(){
        myself.chart.setSize();
      }, stepSize);
      Dashboards.log(Date());

      setTimeout( function(){
        clearInterval( myself._setIntervalHandler );
        Dashboards.log(Date());
      }, lag);
    },
    getMapDefaults: function(){
      return _mapDefaults;
    },
    setMapDefaults: function(newOpts, isExtend){
      if(isExtend){
        $.extend(isExtend, _mapDefaults, newOpts);
      } else {
        _mapDefaults = newOpts;
      }
    },
    interpolateColor: function(minColor,maxColor,maxDepth,depth){
      if(depth === 0){
        return tinycolor(minColor).toHexString();
      }
      if(depth == maxDepth){
        return tinycolor(maxColor).toHexString();
      }
      var color = {},
          minVal = tinycolor(minColor).toRgb(),
          maxVal = tinycolor(maxColor).toRgb();
      _.each(minVal, function(value, key){
        color[key] =  Math.floor(value + (maxVal[key] - value) * (depth/maxDepth) );
      });
      return tinycolor(color).toHexString();
    }
  });

  return MyClass;

})();


/*
 *
 * */
