#!/bin/bash

country="ao"

function create_county_map {
    state_id=$1
    state_key=$2
    statekeylc=$(echo $state_key | perl -ne 'print lc')
    mapname=$(echo $statekeylc | sed 's/-/_/g')
    filename="jquery-jvectormap-$statekeylc-$projection-en.js"

    #echo $state_id $state_key
    meridian=16
    idx=8
    echo "Generating map: $mapname > $filename"

    python ./bjornd-jvectormap-0a0575b/converter/converter.py \
           --where "ID_2 = $state_id" \
           --country_code_index $idx --country_name_index $idx \
           --width 1000 \
           --simplify_tolerance $tolerance \
           --longitude0 $meridian \
           --projection $projection \
           --name $mapname --language en \
           ./AGO_adm/AGO_adm3.shp $folder/$filename
           #--codes_file ./AGO_adm/ao-counties.csv \
}

function create_counties_map {
    #map of angola with all counties
    state_id=$1
    state_key="ao-x-x"
    projection=$3
    statekeylc=$(echo $state_key | perl -ne 'print lc')
    mapname=$(echo $statekeylc | sed 's/-/_/g')
    filename="jquery-jvectormap-$statekeylc-$projection-en.js"

    #echo $state_id $state_key
    meridian=16
    idx=5
    echo "Generating map: $mapname > $filename"

    python ./bjornd-jvectormap-0a0575b/converter/converter.py \
           --country_code_index 5 --country_name_index 5 \
           --codes_file ./AGO_adm/ao-counties.csv \
           --width 1000 \
           --simplify_tolerance $tolerance \
           --longitude0 $meridian \
           --projection $projection \
           --name $mapname --language en \
           ./AGO_adm/AGO_adm3.shp $folder/$filename


}


for tolerance in 0 #10000 1000 100 0
do
    for projection in mill #merc aea lcc
    do
        folder=./maps/$projection-$tolerance/$country
        mkdir -p $folder/

        (create_county_map $1 $2 $projection) &
    done
    wait
done
#cat AGO_adm/ao-states-list.csv  | xargs -n 2 ./ao-counties.sh
