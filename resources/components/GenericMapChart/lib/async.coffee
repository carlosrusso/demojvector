###
Please don't edit the .js file, unless you know what you are doing
This file was produced using CoffeeScript
###

###
parallelQueries = (queries, callback) ->
  result = []
  await
   for q,k in queries
      ex q(), defer result[k] # launch each query
  callback result
###


AsyncComponent = UnmanagedComponent.extend
  superQuery: (ajaxOpts, query, finalCallback, userQueryOptions) ->
    ###
      query: query object
      finalCallback
      ajaxOpts: option object like $.ajaxSettings
        {url:url0, success:function(data){}}
    ###
    await
      @triggerQuery query, (defer resultset), userQueryOptions
      # launch each resource request
      $.ajax _.extend(ajaxOpts, success: defer(data))
    finalCallback resultset, data

  scriptQuery: (url, query, finalCallback, userQueryOptions) ->
      ###
        query: query object
        finalCallback
        ajaxOpts: option object like $.ajaxSettings
          {url:url0, success:function(data){}}
      ###
      await
        @triggerQuery query, (defer resultset), userQueryOptions
        # launch each resource request
        $.getScript url,  defer(data)
      finalCallback resultset, data

  concurrentQuery: (job, query, finalCallback, userQueryOptions) ->
    ###
      query: query object
      finalCallback
      ajaxOpts: option object like $.ajaxSettings
        {url:url0, success:function(data){}}
    ###
    await
      @triggerQuery query, (defer resultset), userQueryOptions
      # launch each resource request
      job defer(data)
    Dashboards.log "Running finalCallback"
    finalCallback resultset, data

  parallelQuery: (query, finalCallback, ajaxOpts) ->
    ###
      query: query object
      finalCallback
      ajaxOpts: array of $.ajaxSettings
        [{url:url0, success:function(data){}}]
    ###
    data = []
    await
      triggerQuery query defer resultset
      for opts,k in ajaxOpts
        opts.success = defer data[k]
        $.ajax opts # launch each resource request
    finalCallback resultset, data
