(function(){
  var dataBar = {
    name: "dataBar2",
    label: "Data Bar",
    defaults: {
      width: undefined,
      //widthRatio:1,
      height: undefined,
      backgroundImage: undefined,
      startColor: undefined,
      endColor: undefined, //"red #448FC8 green yellow",
      stroke: null,
      max: undefined,
      min: undefined,
      includeValue: false,
      absValue: true,
      valueFormat: function(v, format, st, opt, item) {
        // item: header, item, footer
        return "" + sprintf(format || "%.1f",v) ;
      },
      // NEW OPTIONS
      //key: "width", prop: "percentage", unit: "%",
      orientation: 'horizontal',
      //orientation: 'vertical',
      random: false, //if true, generates a fake dataset
      cssSeries: 'series', //each item is associated with a css class: series0, series1, etc...
      fixModel: undefined,
      postExecution: undefined,
      fixModel0: function(model, st, opt){
        model.header = {
          label: 'header',// "Found " + model.items.length + " items",
          style: ''
        };
        model.width = (opt.orientation == 'horizontal') ? _.random(0,100) : 100;
        model.footer = {
          style: '',
          label: sprintf("%d", _.reduce(model.items, function(m, item){return m + item.value;}, 0))
        };
        return model;
      },
      postExecution0: function(model, tgt, st, opt){
        var m = $.extend({}, model); //let's clone the model because it will go away
        var $tgt = $(tgt),
            $tgtHeader = $tgt.prev();

        $tgt.find('.dataBar-body-items').click(function(ev){
          Dashboards.log("Model:" + JSON.stringify(model));
        });

        var mouseover = function(ev){
          $tgt.toggleClass('hover', true);
          $tgtHeader.toggleClass('hover', true);
        }, mouseout = function(ev){
          $tgt.toggleClass('hover', false);
          $tgtHeader.toggleClass('hover', false);
        };
        $tgt.find("*").hover(mouseover, mouseout);
        $tgtHeader.hover(mouseover, mouseout);
      }

    },
    init: function(){
      $.fn.dataTableExt.oSort[this.name+'-asc'] = $.fn.dataTableExt.oSort['numeric-asc'];
      $.fn.dataTableExt.oSort[this.name+'-desc'] = $.fn.dataTableExt.oSort['numeric-desc'];
    },
    implementation: function(tgt, st, opt) {
      var tblMax = Math.max.apply(Math,st.tableData.map(function(e){
                               return e[st.colIdx];
                             })),
          tblMin = Math.min.apply(Math,st.tableData.map(function(e){
                               return e[st.colIdx];
                             }));

      var optMax = parseFloat(opt.max);
      var optMin = parseFloat(opt.min);

      var isValidNumber = function(nr){
        return _.isNumber(nr) && isFinite(nr);
      };

      var validMaxValue = isValidNumber(optMax);
      var validMinValue = isValidNumber(optMin);

      var min, max, val;
      if (opt.absValue){
        max = (validMaxValue == true) ? optMax : Math.max( Math.abs(tblMax), Math.abs(tblMin) );
        min = (validMinValue == true) ? optMin : 0;
        val = Math.abs(parseFloat(st.value));
            min = Math.max(min,0);
      } else {
        max = (validMaxValue == true) ? optMax : Math.max(0, tblMax);
        min = (validMinValue == true) ? optMin : Math.min(0, tblMin);
        val = parseFloat(st.value);
      }

      var cell = $(tgt);
      cell.empty();
      //var $ph =$("<div>&nbsp;</div>").addClass('dataBarContainer').appendTo(cell);
      var $ph =$("<div></div>").addClass('dataBarContainer').appendTo(cell);
      var wtmp = opt.width || $ph.width();
      wtmp *= opt.widthRatio;
      var htmp = opt.height || $ph.height();

      var leftVal  = Math.min(val,0),
          rightVal = Math.max(val,0);

      if (true){
        var me = this;
        var model = this.buildModel(st, opt);
        // Allow the user to correct the model
        if (_.isFunction(opt.fixModel)){
          var fixedModel = opt.fixModel(model, st, opt);
          if (fixedModel){
            model = fixedModel;
          }
        }
        this.saveModel(model, st);
        $ph.append(this.generateBarChart(model, opt));
        if (_.isFunction(opt.postExecution)){
          opt.postExecution(model, tgt, st, opt);
        }
        /*
        $ph.width( '100%' ||
          _.reduce($ph.find('.dataBar-item-label').map(function(idx){
            return $(this).width();
          }), function(memo, w){return memo + w;}, 0)
        );
         */

      } else {
        // xx = x axis
        var xx = pv.Scale.linear(min,max).range(0,wtmp);
        xx = function(x){
          return (x-min)/(max-min)*wtmp;
        };

        var paperSize = xx(Math.min(rightVal,max)) - xx(min);
        paperSize = (paperSize>1)?paperSize:1;
        var paper = Raphael($ph.get(0), paperSize , htmp);
        var c = paper.rect(xx(leftVal), 0, xx(rightVal)-xx(leftVal), htmp);

        c.attr({
          fill: opt.backgroundImage?"url('"+opt.backgroundImage+"')":"90-"+opt.startColor + "-" + opt.endColor,
          stroke: opt.stroke,
          title: "Value: "+ st.value
        });

        if(opt.includeValue) {
          var valph = $("<span></span>").addClass('value').append(opt.valueFormat(st.value, st.colFormat, st, opt));
          valph.appendTo($ph);
        }
      }
    },

    buildModel: function(st, opt){
      var me = this;

      //get values
      var values, total, w;
      if (opt.random){
        // Generate fake values
        values = _.map(_.range(_.random(1,6)), function(v){
          return _.random(1, 1000);
        });
        total = _.reduce(values, function(m,v){return m+v;}, 0);

        if (st.rowIdx < 5){
          opt.orientation = "horizontal";
        } else {
          opt.orientation = "vertical";
        }

      } else {
        // Process data
        if (_.isString(st.value)){
          values = _.map(st.value.split(','), parseFloat);
        } else if (_.isNumber(st.Value)){
          values = [st.value];
        } else {
          values = st.value; //Array
        }
        total = _.reduce(values, function(m,v){return m+v;}, 0);
      }

      w = (opt.max ? total/opt.max*100 : 100);
      var width = (opt.orientation === "horizontal" ? w : (total >0 ? 100 : 0));
      var max = _.max(values);
      var calculatePercentage = function(v){
        return v/total*100;
      };

      var items =_.map(values, function(v, idx){
        return {
          header: undefined,
          value: v,
          css: opt.cssSeries + "" + idx,
          percentage: calculatePercentage(v),
          label: opt.valueFormat(v, "", st, opt, "item"),
          style: me.backgroundCSS(idx, opt)
        };
      });


      //width = (opt.orientation === "horizontal") ? width : 100;

      return {
        css: 'dataBar-' + opt.orientation,//( (Math.random() > 0.5) ? 'horizontal' : 'vertical'), //opt.orientation,
        header:{
          label: undefined,
          style: undefined
        },
        items: items,
        itemsTotal: total,
        bodyStyle: '', //"width: " + _.random(0,100) + "%",
        style: '',
        width: width,
        footer:{
          label: undefined,
          style: undefined
        }
      };
    },
    saveModel: function(model, st){
      if (!st.dataBarModel){
        st.dataBarModel = [];
      }
      if (!st.dataBarModel[st.rowIdx]){
        st.dataBarModel[st.rowIdx] = [];
      }
      st.dataBarModel[st.rowIdx][st.colIdx] = model;
    },

    generateBarChart: function (model, opts) {
      var template = [
        '{{#model}}',
        '<div class="dataBar-container {{css}}" >',
        '  {{#header}} <div class="dataBar-header" style=\"{{style}}\"><div>{{{label}}}</div></div> {{/header}}',
        '  <div class="dataBar-body" style=\"{{bodyStyle}}\">',
        '    <div class="dataBar-body-items" style=\"width:{{width}}%; {{style}}\">',
        '      {{#items}}',
        '      <div class="dataBar-item {{css}}" >',
        '       <div class="dataBar-item-header" >{{{header}}}</div>',
        //'        <div class="dataBar-item-bar" style="{{opts.key}}:{{' + opts.prop + '}}{{opts.unit}}; {{style}}" >',
        '        <div class="dataBar-item-bar" style="width:{{percentage}}%; {{style}}" >', //no need for opts.key, opts.prop and opts.unit
        '        {{#label}}<div class="dataBar-item-label">{{label}}</div>{{/label}}',
        '        </div>',
        '      </div>',
        '      {{/items}}',
        '    </div>',
        '    {{#footer}}',
        '    <div class="dataBar-body-footer" >',
        '      <div class="dataBar-footer-dimension dataBar-footer-label" style=\"{{style}}\">{{{label}}}</div>',
        '    </div>',
        '    {{/footer}}',
        '  </div>',
        '  {{#footer}}',
        '  <div class=\"dataBar-footer-dimension dataBar-footer\">{{{label}}}</div>', //reserve space for footer
        '  {{/footer}}',
        '</div>',
        '{{/model}}',
        ''].join('');
      return Mustache.render(template, {
        model: model,
        opts: opts
      });
    },
    backgroundCSS: function(idx, opt){

      if (opt.backgroundImage){
        return "background:url('"+opt.backgroundImage+"'); ";
      }
      if (!_.isString(opt.startColor)){
        return undefined;
      }

      var startColor = opt.startColor ? (opt.startColor.split(" ")[idx] || undefined) : undefined,
          stopColor = opt.endColor ? (opt.endColor.split(" ")[idx] || undefined) : undefined;

      if (stopColor === undefined){
        return "background:"+ startColor + "; ";
      } else {
        // Perhaps this feature is not needed
        return Mustache.render([
          "background-image:-webkit-linear-gradient(-90deg, {{startColor}} 0,{{stopColor}} 100%); ",
          "background-image:-moz-linear-gradient(180deg, {{startColor}} 0, {{stopColor}} 100%); ",
          "background-image:linear-gradient(180deg, {{startColor}} 0, {{stopColor}} 100%); "
        ].join(''), {
          startColor: startColor,
          stopColor: stopColor
        });
      }
    }
  };
  Dashboards.registerAddIn("Table", "colType", new AddIn(dataBar));





})();
