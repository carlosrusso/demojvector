#!/bin/bash

prefix=$1 #USA_adm
country=$2 #us

function create_country_map {
    mapname=$(echo $country | perl -ne 'print lc' | sed 's/-/_/g')
    filename="jquery-jvectormap-$country-$projection-en.js"

    meridian=-100
    idx=3
    admlevel=1
    case $country in
        "ca") #canada
            meridian=-90
            ;;
        "br") #brazil
            meridian=-47
            ;;
        "mx") #mexico
            meridian=-100
            ;;
        "us-mp") #mariana islands
            meridian=146
            idx=0
            admlevel=0
            ;;
        "us-gu") #guam
            meridian=144.8
            idx=0;
            admlevel=0
            ;;
        "us-pr") #puerto rico
            meridian=-66
            ;;
        "us-vi") # virgin islands
            meridian=-65
            ;;
        "pw") #palau
            meridian=134.5
            admlevel=0
            idx=0
            ;;
        "ao") #angola
            meridian=16


    esac
    echo "Generating map: $mapname > $filename"

    python ./bjornd-jvectormap-0a0575b/converter/converter.py \
           --country_code_index $idx --country_name_index $idx \
           --codes_file ./$prefix/$country-states.csv --width 1000 \
           --simplify_tolerance $tolerance \
           --longitude0 $meridian \
           --projection $projection \
           --name $mapname --language en \
           ./$prefix/$prefix$admlevel.shp $folder/$filename
}


for tolerance in 10000 1000 100 0
do
    for projection in mill merc aea lcc
    do
        folder=./maps/$projection-$tolerance
        mkdir -p $folder/

        (create_country_map $prefix $country) #&
    done
    #wait
done
# ./countries.sh AGO_adm ao
