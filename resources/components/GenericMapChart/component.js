/*

 Regions are coded using ISO 3166-2: PT, US, FR, UK, DE, etc

 Implemented as a wrapper over JVectorMap

 API changes (most recent on top):
   * .buckets -> moved to map options ._defaults.buckets
   * Markers -> moved to map options: ._defaults.Markers
   * .getMapDefaults -> .getMapOptions
   * .setMapDefaults -> .setMapOptions (options are not defaults)

 Notes for the future
  * .setParameterCallbacks() hooks to regionClick and triggers Dashboards.processChange

 */



var GenericMapChartComponent = (function (){
  // these settings are shared by all instances!
  var _defaults = { //Fallback properties, in case CDE does not define them
    mapId: 'world_mill_en',
    isoFormat: true,
    minColor: "#E5E4E4",
    maxColor: "#EF720B",
    scaleSteps: 10,
    scaleSignificantDigits: 2,
    loadInParallel: true
  };

  // var Marker = {
  //   attribute: 'r',
  //   scale: [5, 20]
  // };

  var MyBaseClass = UnmanagedComponent.extend({
    _options: {},
    _defaults: {
      // move all defaults here
      zoomButtons: true,
      zoomOnScroll: true,
      regionsSelectable:true,
      backgroundColor: "#FFFFFF",
      regionStyle: {
        initial: {
          "fill": '#EEEDED',
          'stroke': '#9a9a9a'
        },
        hover: {
          // 'fill': '#d4e4eb',
          'stroke':'#3b7ed8'
        }
      },
      buckets: [{
        css:'series0',
        visible: true,
        min: -Infinity,
        max: +Infinity
      }],
      markerStyle: {
        default: {
          attribute: 'r',
          scale: [5, 20]
        },
        initial: {
          'fill': '#aa0fc1'
        },
        hover: {
          //'fill': '#00ff04'
        }
      },
      onRegionOver: function (event, code, region) {
        document.body.style.cursor = "pointer";
      },
      onRegionOut: function (event, code, region) {
        document.body.style.cursor = "default";
      },
      onMarkerOver: function (event, code, region) {
        document.body.style.cursor = "pointer";
      },
      onMarkerOut: function (event, code, region) {
        document.body.style.cursor = "default";
      }
    },
    update: function() {
      this.preExec();
      //this.devSandbox();
      var qd = $.extend({}, this.chartDefinition), //queryDefinition
          //TODO: split chartDefinition into queryDefinition and mapDefinition
          callback = _.bind(this.metaRender, this),
          url = this.getMapURL(),
          myself = this;

      if ( this.isMapLoaded() ){
        this.triggerQuery( qd, callback );
      } else {
        if (this.get('loadInParallel')){
          // Full asynchronous operation.
          // Load a map in parallel with loading the data.
          // note that preExec was already run
          this.deferredTriggerQuery( qd, [ $.getScript(url) ], callback);
        } else {
          $.getScript(url).then( function(){
            // Load a map, then load the data
            // this will run preExecution again... a minor price to pay
            myself.triggerQuery( qd, callback );
          });
        }
      }
    },
    isMapLoaded: function(mapId){ return false;},
    getMapURL: function(){ return '';},
    importMapData: function(){
      this.mapData = {'key': {name: 'name'}};
    },

    deferredTriggerQuery: function (queryDef, deferreds, callback, userQueryOptions) {
      /*
       * Variation of the triggerQuery, supporting deferreds
       * The triggerQuery lifecycle handler builds a lifecycle around Query objects.
       *
       * It takes a query definition object that is passed directly into the Query
       * constructor, and the component rendering callback, and implements the full
       * preExecution->block->render->postExecution->unblock lifecycle. This method
       * detects concurrent updates to the component and ensures that only one
       * redraw is performed.
       */

      var silent = this.isSilent();
      if (!silent){
        this.block();
      };
      userQueryOptions = userQueryOptions || {};
      /*
       * The query response handler should trigger the component-provided callback
       * and the postExec stage if the call wasn't skipped, and should always
       * unblock the UI
       */
      // var success = _.bind(function(data) {
      //   // callback(data);
      //   //this.postExec();
      // }, this);
      var always = _.bind(function (){
        if (!silent){
          this.unblock();
        }
      }, this);
      // var handler = this.getSuccessHandler(success, always);

      var errorHandler = this.getErrorHandler();

      var query = this.queryState = this.query = Dashboards.getQuery( queryDef );
      var ajaxOptions = {
        async: true
      };
      if(userQueryOptions.ajax) {
        _.extend(ajaxOptions,userQueryOptions.ajax);
      }
      query.setAjaxOptions(ajaxOptions);
      if(userQueryOptions.pageSize){
        query.setPageSize(userQueryOptions.pageSize);
      }


      // Setup a function that launches a query and returns a deferred object
      // Each deferred object is effectively a task that runs in parallel
      var myself = this;
      var launchQuery = function(){
        var _deferredQuery = $.Deferred();
        query.fetchData(myself.parameters, function(){
          // handler.apply(myself, arguments);
          _deferredQuery.resolve.apply(myself, arguments);
        }, function(){
          errorHandler.apply(myself, arguments);
          _deferredQuery.reject();
        });
        return _deferredQuery.promise().done(always); // danger: is this done or always?
      };

      var deferredSuccess = function(datasets){
        if (_.isFunction(myself.postFetch)){
          datasets = myself.postFetch(datasets);
        }
        if ( _.isFunction(callback) && (datasets !== false) ){
          callback(datasets);
        }
        myself.postExec();
      };

      // Launch all tasks, run postExec once all are successful
      deferreds = deferreds || [];
      deferreds.unshift( launchQuery() ); //prepend
      return $.when.apply(null, deferreds).then( deferredSuccess );
    },



    metaRender: function(data) {
      /* Establish basic machinery
       *
       */
      this.importMapData();
      var values = this.prepareData(data);
      this.setRange(values);

      // Workaround for a bug in JVectorMap: it seems not to respect an externally supplied series.min
      // when we have data only on a single key. Adding a fake key set to this._min seems to solve it.
      // This may actually have been fixed already in one of the merges, we need to check that
      values['nothing.min'] = this._min;
      values['nothing.max'] = this._max;

      this.$container = $("<div />").addClass('jqvm-container')
        .appendTo(
          this.placeholder().empty()
        );
      this.$container.empty();

      //Setup width/height programmatically
      var w = this.get('width'),
          h = this.get('height');
      w = ( _.isNumber( w ) && w+'px' )  ||  ( _.isEmpty(w) && '100%' )  ||
        ( _.isString( w ) && w )       ||  '100%';
      h = ( _.isNumber( h ) && h+'px' )  ||  ( _.isEmpty(h) && '100%' )  ||
        ( _.isString( h ) && h )       ||  '100%';
      this.$container.css({ width:w , height: h });

      this.values = values;
      this.render(); //delegate drawing to subclass
      //this.drawScale(values); //scale should be called from within render
      this.setupCallbacks();
      this.setupParameterCallbacks();
    },
    render: function(){
      // override component-specific stuff here. Data is in this.values
    },

    renderSeries: function(visibleSeries){
      var opts = $.extend(true, {}, this.getMapOptions());
      this.$container.empty();
      _.each(opts.buckets, function(bucket){
        bucket.visible = visibleSeries ? _.contains(visibleSeries, bucket.css) : true;
      });
      this.render();
    },

    getBucket: function(code){
      // Returns the bucket in which the region/marker is stored.
      var opts = $.extend(true, {}, this.getMapOptions());
      var value = this.values[code];
      var bucket = _.compact(_.map(opts.buckets, function(bucket, idx){
        if ((value >= (bucket.min || -Infinity)) && (value <= (bucket.max || Infinity))){
          return bucket;
        } else {
          return false;
        }
      }));
      return bucket[0] || {};
    },

    setRange: function(values){
      this._min = _.isNumber(this.get('min')) ? this.get('min') : _.min(values);
      this._max = _.isNumber(this.get('max')) ? this.get('max') : _.max(values);

      var scaleSignificantDigits = this.get('scaleSignificantDigits');

      if (scaleSignificantDigits !== undefined && scaleSignificantDigits >= 0){
        var roundScale = function (n, sig) {
          var mult = Math.pow(10, Math.abs(sig) - Math.floor(Math.log(n) / Math.LN10) - 1);
          if (_.isNaN(mult)){
            mult = 1;
          }
          var val = function(p) {return Math.round(Math.round(n * mult + p) / mult);};
          if (val(0) == n){
            return n;
          } else {
            return val(0.5) || 0;
          }
        };
        this._min = roundScale(this._min, -scaleSignificantDigits);
        this._max = roundScale(this._max, scaleSignificantDigits);
      }
    },
    drawScale: function(values){
      var opts = this.getMapOptions(); // read-only, no need to do a deep-extend
      if (opts.buckets && opts.buckets.length > 1){
        return this._drawScaleCategories(values);
      } else {
        return this._drawScaleContinuous(values);
      }
    },

    _drawScaleCategories: function(values){
      var opts = this.getMapOptions(); // read-only, no need to do a deep-extend
      var template = [
        '<div class="map-scale-container map-scale-container-categoric">',
        ' {{#header}} <div class=\"map-scale-header\">{{{header}}}</div>{{/header}}',
        '  <div class=\"map-scale-body\">',
        '    {{#items}}',
        '    <div class=\"map-scale-item\">',
        '      <div class=\"map-scale-item-icon {{css}}\"></div>',
        '      <div class=\"map-scale-item-label {{css}}\">{{{label}}}</div>',
        '    </div>',
        '    {{/items}}',
        '  <div/>',
        '  {{#footer}}<div class=\"map-scale-footer\">{{{footer}}}</div>{{/footer}}',
        '<div/>',
      ''].join('');

      var html = Mustache.render(template, {
        header: undefined,
        items: opts.buckets,
        footer: undefined
      });

      this.$scale = $(html).appendTo(this.$container);
    },


    _drawScaleContinuous: function( values) {
      var steps = this.get('scaleSteps'),
          color, i;
      if (Math.floor(steps) == 0){
        return;
      } else if (steps < 0){
        steps = 1;
      }

      var minValue, maxValue, title;
      var minColor = this.get('minColor'),
          maxColor = this.get('maxColor');

      var ft = this.get('valueFormat');
      if( _.isFunction( ft ) ) {
        minValue = ft(this._min);
        maxValue = ft(this._max);
      } else {
        minValue = sprintf('%d', this._min);
        maxValue = sprintf('%d', this._max);
      }

      var template = [
        '<div class="map-scale-container map-scale-container-continuous jqvm-scale" >',
        ' {{#header}} <div class=\"map-scale-header\">{{{header}}}</div>{{/header}}',
        '  <div class=\"map-scale-body\">',
        '    <div class=\"min-value\">{{minValue}}</div>',
        '    {{#items}}',
        '    <div title=\"{{{title}}}\" class=\"map-scale-item {{css}}\" style=\"{{style}}\">',
        '      <div class=\"map-scale-item-icon\"></div>',
        '      <div class=\"map-scale-item-label\">{{{label}}}</div>',
        '    </div>',
        '    {{/items}}',
        '    <div class=\"max-value\">{{maxValue}}</div>',
        '  <div/>',
        '  {{#footer}}<div class=\"map-scale-footer\">{{{footer}}}</div>{{/footer}}',
        '<div/>',
        ''].join('');

      var me = this;
      var html = Mustache.render(template, {
        header: undefined,
        minValue: minValue,
        maxValue: maxValue,
        items: _.map(_.range(steps), function(idx){
          var style = [
            "width: {{width}};",
            "background: {{maxColor}}; ",
            $.browser.mozilla ? "background-image: -moz-linear-gradient(left, {{minColor}} 0%, {{maxColor}} 100%);" : "",
            $.browser.webkit  ? "background-image: -webkit-gradient(linear, left top, right top, color-stop(0% {{minColor}}), color-stop(100% {{maxColor}}));" : "",
            $.browser.webkit  ? "background-image: -webkit-linear-gradient(left, {{minColor}} 0%,  {{maxColor}} 100%);" : "",
            $.browser.opera ? "background-image: -o-linear-gradient(left, {{minColor}} 0%,  {{maxColor}} 100%);" : "",
            $.browser.msie ? "background-image: -ms-linear-gradient(left, {{minColor}} 0%,  {{maxColor}} 100%);" : "",
            "background-image: linear-gradient(left, {{minColor}} 0%,  {{maxColor}} 100%);",
            $.browser.msie && parseFloat($.browser.version) < 9 ? "filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='{{minColor}}', endColorstr='{{maxColor}}', GradientType=1);" : ""
          ];
          var nVal  = me._min + (me._max - me._min) * (idx/(steps));
          var nVal2 = me._min + (me._max - me._min) * ((idx+1)/(steps));
          var title = sprintf('%d - %d',nVal, nVal2);
          return {
            css: "scale-item",
            style: Mustache.render(style.join(' '), {
              width: 100/(steps) + "%",
              minColor: me.interpolateColor( minColor, maxColor, steps, idx),
              maxColor: me.interpolateColor( minColor, maxColor, steps, idx+1)
            }),
            title: title,
            label: ''
            //label: '&nbsp;'
          };
        }),
        footer: undefined
      });

      this.$scale = $(html).appendTo(this.$container);
    },

    buildMapModel: function(values){},

    _callbacks: [
      // Use same callback names as in JVectorMap
      "regionCreated", "regionLabelShow", "regionOver", "regionOut", "regionClick", "regionSelected",
      "markerCreated", "markerLabelShow", "markerOver", "markerOut", "markerClick", "markerSelected",
      "viewportChange"
    ],
    setupCallbacks: function(){
    },
    setupParameterCallbacks: function(){
      var myself = this;
      // Legacy code, pre-Unmanaged. Store the last selected marker in a parameter
      this.on('regionSelected markerSelected',function(ev, code, isSelected, selection) {
        if (Dashboards.debug > 1){
          Dashboards.log("region/marker " + code + " was selected.", "debug");
        }
        myself.lastValues = (myself.get('isoFormat').toLowerCase() == "true") ? selection : selection.map(function(v){
          return myself.mapData[v].name;
        });
        Dashboards.processChange(myself.name);
      });

      // Update the parameter with the key/name of the selected region/marker
      this.on('regionClick  markerClick',function(ev, code) {
        if (Dashboards.debug > 1){
          Dashboards.log("region/marker " + code + " was clicked.", "debug");
        }
        if (!_.isUndefined(myself.parameter) ) {
          Dashboards.fireChange(myself.parameter, code);
        }
      });
    },

    getValue: function() { // Return last selected region/marker
      return this.lastValues;
    },
    setValues: function (values){},

    prepareData: function(data) {
      /** Matches resultset from query with the keys/names in the map

       */
      var mapKeys = {},
          values = {},
          myself = this,
          isoFormat = this.get('isoFormat').toLowerCase() == "true"; //if not isoFormat, map "Alabama" -> "US-AL"

      // Match incoming data to the keys used by the map.
      // The map metadata is supposed to be of the form: { "US-AL": {name: "Alabama", ...}}
      _.each( this.mapData, function(el, key){
        mapKeys[ isoFormat ? key : el.name] = key;
        //data[key] = Math.random()*1000;
      });

/*

      // Create a "values" object = {mapkey: value}
      myself.allSeries = {}; // this will hold all columns except for the first two (key, value)
      _.each( data.resultset, function(row){
        var key = row[0];
        if( key in mapKeys) {
          values[mapKeys[key]] = row[1]; // data[] holds a single scalar value. A better idea would be to modify jvectormap to allow an array (and use the first element)
          myself.allSeries[mapKeys[key]] = [];
          _.each(row.slice(1), function(el){
            myself.allSeries[mapKeys[key]].push(el);
          });
        }
      });
*/
      // Create a "values" object = {mapkey: {value: X, ...}}
      // Guess the names from the metadata.
      var parsedData = {};
      var columns = [], columnsLC = [];
      _.each(data.metadata, function(el){
        columns[el.colIndex] =  el.colName;
        columnsLC[el.colIndex] =  el.colName.toLowerCase();
      });
      var idxKey = _.indexOf(columnsLC, 'key');
      var idxValue = _.indexOf(columnsLC, 'value');
      idxKey = idxKey >=0 ? idxKey : 0;
      idxValue = idxValue >=0 ? idxValue : 1;


      _.each( data.resultset, function(row){
        var key = row[idxKey],
            value = row[idxValue],
            item = {};
        if( key in mapKeys) {
          item.value = value; // we must first guarantee that there is a "value" field.
          _.each(row, function(el, idx){
            item[columns[idx]] = el;
          });
          parsedData[mapKeys[key]] = item;
        }
      });
      this.parsedValues = _.object(_.keys(parsedData), _.pluck(parsedData, 'value'));
      this.parsedData = parsedData;


      // Populate markerDefinitions from the datasource (UNTESTED)
      if ( _.indexOf(['markers', 'both'], this.get('mapMode')) >= 0){
        // var hasX = function(X){
        //   return _.find(data.metadata, function(el){
        //     return el.colName.toLowerCase() == X ? el.colIndex : undefined;
        //   });
        // };
        var hasX = function(X){
          return _.indexOf(_.map(columns, function(el){return el.toLowerCase();}), X);
        };
        var hasLatitude= hasX('latitude') || hasX('lat');
        var hasLongitude = hasX('longitude') || hasX('lon') || hasX('lng');
        if ((hasLatitude >=0)  && (hasLongitude >=0) ){
          _.each(data.resultset, function(row){
            var item = {};
            item[row[idxKey]]  =  {
              value: row[idxValue],
              latLng: [ row[hasLatitude], row[hasLongitude] ]
            };
            myself.markerDefinitions = $.extend(true, myself.markerDefinitions, item);
          });
        }
      }


      return this.parsedValues; // {"US-AL": value}
    },

    setSize: function (lag, steps){
      steps = ( _.isNumber(steps) && (steps>=1) && steps) || 20;
      lag =  _.isNumber(lag) && (lag>0) && lag;
      var stepSize = lag / steps,
          myself = this;

      clearInterval( this._setIntervalHandler);
      myself.chart.setSize();

      this._setIntervalHandler = setInterval( function(){
        myself.chart.setSize();
      }, stepSize);
      Dashboards.log(Date());

      setTimeout( function(){
        clearInterval( myself._setIntervalHandler );
        Dashboards.log(Date());
      }, lag);
    },
    get: function(opt){
      // Obtain some component option, abstract where it came from
      var options = $.extend(true, {}, this._defaults, this.chartDefinition, this.mapDefinition, this._options);
      return options[opt];
    },
    getMapDefaults: function(){
      Dashboards.log(this.name  + ".getMapDefaults is deprecated. Use .getMapOptions instead.", "debug");
      return this.getMapOptions();
    },
    getMapOptions: function(){
      return $.extend(true, {}, this._defaults, this._options);
    },
    setMapDefaults: function(newOpts, isExtend){
      Dashboards.log(this.name  + ".setMapDefaults is deprecated. Use .setMapOptions instead.", "debug");
      if(isExtend){
        this._defaults = $.extend(true, {}, this._defaults, newOpts);
      } else {
        this._defaults = newOpts;
      }
    },
    setMapOptions: function(newOpts, isExtend){
      if(isExtend){
        this._options = $.extend(true, {}, this._options, newOpts);
      } else {
        this._options = newOpts;
      }
    },
    interpolateColor: function(minColor,maxColor,maxDepth,depth){
      if(depth === 0){
        return tinycolor(minColor).toHexString();
      }
      if(depth == maxDepth){
        return tinycolor(maxColor).toHexString();
      }
      var color = {},
          minVal = tinycolor(minColor).toRgb(),
          maxVal = tinycolor(maxColor).toRgb();
      _.each(minVal, function(value, key){
        color[key] =  Math.floor(value + (maxVal[key] - value) * (depth/maxDepth) );
      });
      return tinycolor(color).toHexString();
    }
  });


  /********************************************************************************
   *
   *   Implementation using JvectorMap
   *
   ********************************************************************************/


  var MapComponentJVM = MyBaseClass.extend({
    isMapLoaded: function(mapId){
      if (!mapId){
        mapId = this.get('mapId');
      }
      return (mapId in jvm.WorldMap.maps);
    },
    getMapURL: function(){
      var url,
          mapLocation = this.get('mapLocation'),
          mapId = this.get('mapId');
      if (mapLocation.endsWith('.js')) {
        url = mapLocation; // allows hacking by specifying the file directly
        this.mapDefinition.mapId = url.split('jquery-jvectormap-')[1].split('.js')[0].split('-').join('_');
      } else {
        url = mapLocation + '/jquery-jvectormap-' + mapId.split('_').join('-') + '.js';
      }
      return url;
    },
    importMapData: function(){
      var myself = this;
      this.mapData = {};
      _.each(jvm.WorldMap.maps[this.get('mapId')].paths, function(el, key){
        myself.mapData[key] = {name: el.name};
      });
    },

    customMarker: function(canvas, marker, markerStyle, markersGroup){
      var max = 1000, min=0;
      //Dashboards.log('Rendering marker');
        switch(2){
        case 1:
          var el = new jvm[canvas.classPrefix + 'CircleElement'](marker, markerStyle);
          canvas.add( el, markersGroup);
          return el;
        case 2:

          var f = Snap.fragment('<?xml version="1.0" encoding="UTF-8" standalone="no"?> <!-- Created with Inkscape (http://www.inkscape.org/) --> <svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb"xmlns:dc="http://purl.org/dc/elements/1.1/"xmlns:cc="http://creativecommons.org/ns#"xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"xmlns:svg="http://www.w3.org/2000/svg"xmlns="http://www.w3.org/2000/svg"xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"width="31.411081"height="59.017464"id="svg2"version="1.1"inkscape:version="0.48.2 r9819"sodipodi:docname="marker2.svg"> <defs id="defs4"> <linearGradient id="linearGradient5222"osb:paint="solid"> <stop style="stop-color:#000000;stop-opacity:1;"offset="0"id="stop5224" /> </linearGradient> <inkscape:perspective sodipodi:type="inkscape:persp3d"inkscape:vp_x="0 : 526.18109 : 1"inkscape:vp_y="0 : 1000 : 0"inkscape:vp_z="744.09448 : 526.18109 : 1"inkscape:persp3d-origin="372.04724 : 350.78739 : 1"id="perspective2985" /> </defs> <sodipodi:namedview id="base"pagecolor="#ffffff"bordercolor="#666666"borderopacity="1.0"inkscape:pageopacity="0.0"inkscape:pageshadow="2"inkscape:zoom="8.1017786"inkscape:cx="19.448122"inkscape:cy="89.28231"inkscape:document-units="px"inkscape:current-layer="layer1"showgrid="false"inkscape:window-width="1276"inkscape:window-height="756"inkscape:window-x="4"inkscape:window-y="22"inkscape:window-maximized="0"fit-margin-top="0"fit-margin-left="0"fit-margin-right="0"fit-margin-bottom="0" /> <metadata id="metadata7"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1"inkscape:groupmode="layer"id="layer1"> <text xml:space="preserve"style="font-size:10px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"x="-7.0629263"y="-50.223419"id="text3797"sodipodi:linespacing="125%"><tspan sodipodi:role="line"id="tspan3799"x="-7.0629263"y="-50.223419"style="font-size:14px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-family:Open Sans;-inkscape-font-specification:Open Sans"><tspan style="font-size:24px"id="tspan5230">99</tspan><tspan style="font-size:8px"id="tspan5228">%</tspan></tspan></text> <g id="g3785"style="fill:#ff8a00;fill-opacity:1;stroke:#000000;stroke-width:1.0003159;stroke-linejoin:round;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none"transform="matrix(0.54062415,0,0,1,-0.12340383,-0.12345554)"> <path inkscape:connector-curvature="0"id="path3783"d="M 0.24688523,0.13533685 0,-48.37761 29.255899,-48.624496 z"style="fill:#ff8a00;fill-opacity:1;stroke:none" /> </g> </g> </svg>');
          var p = Snap(); //Snap(canvas.container);
          p.attr({
            'data-index': marker['data-index']
          });
          p.isHovered = false;
          //p.style = markerStyle;
          p.style = markerStyle;
          p.append(f.select('svg g'));
          p.remove = function(){};
          p.addClass = function( className ){
            this.node.setAttribute('class', className);
          };
          p.setStyle = function( style, skipSaving){
            // It seems that jvectormap also uses the syntax setStyle(attribute, value)

            //Dashboards.log('setStyle called: ' + JSON.stringify(style), 'debug');
            if (!_.isObject(style)){
              var a = {};
              a[arguments[0]] = arguments[1];
              style = a;
            }
            if (! (skipSaving===true) ){
              this.style.current = $.extend(true, this.style.current, style);
            }
            //Dashboards.log('Current style: ' + JSON.stringify(this.style));

            // Place the marker
            if (('x' in style) && ('y' in style)){
              p.select('#layer1').transform("").transform(new Snap.Matrix().translate(style.x,style.y));
            }

            // Modify the marker
            var symbol = p.select('#g3785'),
                text = p.select('tspan');
            _.each(style, function(value, key){
              switch (key){
              case 'fill':
                //$(symbol.node).css('fill', value);
                symbol.select('path').attr({fill: value});
                break;
              case 'value':
              //case 'r':
                //p.select('g text tspan').node.innerHTML = value + "%";
                p.select('#tspan5230').node.firstChild.nodeValue = sprintf('%d', (value -min)/(max -min)*100);
                symbol.transform("").transform(new Snap.Matrix().scale(1, (value - min)/(max - min)));
                text.attr({y: symbol.getBBox().y - 10});
                break;
              }
            });
          };
          p.setHovered = function(isHovered){
            if (this.isHovered != isHovered) {
              this.isHovered = isHovered;
              //Dashboards.log('Marker is ' + (isHovered ? '' : 'not ') + 'hovered');
              this.setStyle( isHovered ? this.style.hover : this.style.current, true);

              var classes = this.node.getAttribute("class").split(" ");
              if (isHovered){
                $(classes).map(function(idx, el){ return el == "normal" ? "" : el; });
                classes.push("hover");
              } else {
                $(classes).map(function(idx, el){ return el == "hover" ? "" : el; });
                classes.push("normal");
              }
              this.node.setAttribute('class', classes.join(" "));
              /*
              if (isHovered){
                this.node.classList.add('hover');
              } else {
                this.node.classList.remove('hover');
              }*/
            }

          };

          canvas.add(p, markersGroup);
          p.setStyle( p.style.initial );
          Dashboards.log('Setting marker initial style: ' + JSON.stringify(p.style.initial));
          return p;
           case 3:
          var el2 = new jvm[canvas.classPrefix + 'ShapeElement']('rect', {x:marker.x, y:marker.y, width:30, height:50}, {fill:'red', stroke:'blue'});
          canvas.add(el2, markersGroup);
          return el2;
        }
      return undefined;
    },
    devSandbox: function(){
      var myself = this;
      var g = function(canvas, marker, style, markersGroup){
        Dashboards.log('Rendering another marker using the original style');
        return canvas.addCircle(marker, style, markersGroup);
      };
      /*
      this.markerDefinitions = {
        "AO-BGU":{name:"Benguela",latLng: [-12.5790048,13.4037117] }
      };
       */
      // Add custom renderer
      _.each(this.markerDefinitions, function (el, key){
        el.renderer = myself.customMarker;
      });
      //jvm.WorldMap.maps = {}; //DEBUG: remove all preloaded maps
    },



    render: function(){
      this.$container.vectorMap( this.buildMapModel(this.values) );
      this.chart = this.$container.vectorMap( 'get' , 'mapObject');
      this.drawScale();
    },

    setupCallbacks: function(){

      // Add JVectormap callbacks
      _.each(jvm.WorldMap.apiEvents, _.bind(function(event, fn){
        var myself = this;
        myself.$container.bind(event + '.jvectormap', function(){
          myself.trigger.apply(myself, _.union([event], arguments)); //hook jvectormap events to Backbone
        });
        myself.off(event); //unhook previous listeners
        if (_.isFunction(myself[fn])){
          // hook up functions using the name convention onRegionClick
          Dashboards.log("Jvectormap event: " + event);
          myself.on(event, myself[fn]);
        }
      }, this));

    },

    buildMapModel: function(values){
      var myself =  this,
          opts = $.extend(true, {}, this.getMapOptions()),
          mapMode = this.get('mapMode').toLowerCase();

      //Dashboards.log('Opts: ' + JSON.stringify(opts).replace(',', ',\n'));
      var minColor = tinycolor(this.get('minColor')).toHexString(),
          maxColor = tinycolor(this.get('maxColor')).toHexString();

      opts.map = this.get('mapId');
      opts.series = {};

      if ( _.indexOf(['markers', 'both'], mapMode) >= 0){
        opts.markers = {};
        if (_.isObject(this.markerDefinitions)) {
          _.each(values, function(val, key){
            if (key in  myself.markerDefinitions){
              opts.markers[key] = myself.markerDefinitions[key];
            }
          });
        } else if (_.isArray(this.markerDefinitions)){
          _.each(values, function(row){
            opts.markers[row[0]] = row[1];
          });
        }

        var markerOpts = [],
            usingCustomRenderer = _.some(opts.markers, function(v){
              return _.has(v, 'renderer');
            }),
            usingDefaultRenderer = _.some(opts.markers, function(v){
              return !_.has(v, 'renderer');
            });
        if ( usingCustomRenderer ){
          markerOpts.push({
            attribute: "value",
            scale: [ this._min, this._max ],
            values: values,
            min: this._min,
            max: this._max
          });
        }
        if ( usingDefaultRenderer ){
          markerOpts.push( $.extend(true, {}, opts.markerStyle.default, {
            values: values,
            min: this._min,
            max: this._max
          }));
        }
        opts.series.markers = markerOpts;
      }

      if ( _.indexOf(['regions', 'both'], mapMode) >=0 ){
        opts.series.regions = [{
          attribute: "fill",
          scale: [ minColor, maxColor ],
          values: values,
          min: this._min,
          max: this._max
        }];
      }


      var me = this;
      if ( opts.series.regions && (opts.series.regions.length == 1) && !_.isEmpty(opts.buckets)){
        var regions = opts.series.regions;
        opts.series.regions = _.compact(_.map(opts.buckets, function(bucket){
          var bucketValues = {};

          if (bucket.visible === false){
            return false;
          }

          _.each(values, function(val, key){
            if ( (val >= (bucket.min || -Infinity)) && (val <= (bucket.max || Infinity)) ){
              bucketValues[key] = val;
            }
          });
          return {
            attribute: "fill",
            scale: [
              tinycolor( _.isArray(bucket.colors) ? bucket.colors[0] : minColor).toHexString(),
              tinycolor( _.isArray(bucket.colors) ? bucket.colors[1] : maxColor).toHexString()
            ],
            values: bucketValues,
            min: _.isFinite(bucket.min) ? bucket.min : me._min,
            max: _.isFinite(bucket.max) ? bucket.max : me._max
          };
        }));
      }

      // // Update the parameter with the key/name of the selected region/marker
      // opts.onRegionSelected = function(ev, code, isSelected, selection) {
      //   myself.lastValues = (myself.get('isoFormat').toLowerCase() == "true") ? selection : selection.map(function(v){
      //     return myself.mapData[v];
      //   });
      //   Dashboards.processChange(myself.name);
      // };
      // opts.onMarkerSelected = function(ev, code, isSelected, selection) {
      //   myself.lastValues = (myself.get('isoFormat').toLowerCase() == "true") ? selection : selection.map(function(v){
      //     return myself.mapData[v];
      //   });
      //   Dashboards.processChange(myself.name);
      // };

      return $.extend(true, opts, myself.get('extraOpts'));
   },

    setValues: function (values){
      var newValues = this.prepareData(values);
      var myself = this,
          mapMode = this.get('mapMode').toLowerCase();

      if ( _.indexOf(['regions', 'both'], mapMode) >= 0){
        _.each(myself.chart.series.regions, function(val){
          val.setValues(values);
        });
      }

      if ( _.indexOf(['markers', 'both'], mapMode) >= 0){
        _.each(myself.chart.series.markers, function(val){
          val.setValues(values);
        });
      }

    },

    setSize: function (lag, steps){
      steps = ( _.isNumber(steps) && (steps>=1) && steps) || 20;
      lag =  _.isNumber(lag) && (lag>0) && lag;
      var stepSize = lag / steps,
          myself = this;

      clearInterval( this._setIntervalHandler);
      myself.chart.setSize();

      this._setIntervalHandler = setInterval( function(){
        myself.chart.setSize();
      }, stepSize);
      Dashboards.log(Date());

      setTimeout( function(){
        clearInterval( myself._setIntervalHandler );
        Dashboards.log(Date());
      }, lag);
    }
  });

  return MapComponentJVM;

})();


/*
 *
 * */
