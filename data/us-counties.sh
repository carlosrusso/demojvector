#!/bin/bash

function create_us_state_map0 {
    state_id=$1
    state_key=$2
    projection=$3
    statekeylc=$(echo $state_key | perl -ne 'print lc')
    mapname=us_"$statekeylc"
    filename="jquery-jvectormap-us-$statekeylc-$projection-en.js"

    meridian=-100
    case $state_key in
        "AK") #alaska
            meridian=-130
            ;;
        "HI") #hawaii
            meridian=-150
            ;;
    esac
    echo "Generating map: $mapname > $filename"

    python ./bjornd-jvectormap-0a0575b/converter/converter.py \
           --where "ID_1 = $state_id" \
           --country_code_index 5 --country_name_index 5 \
           --codes_file ./USA_adm/USA_adm2_us-state-fips.csv --width 1000 \
           --simplify_tolerance $tolerance \
           --longitude0 $meridian \
           --projection $projection \
           --name $mapname --language en \
           ./USA_adm/USA_adm2.shp $folder/$filename
}


function create_us_state_map {
    state_id=$1
    state_key=$2
    statekeylc=$(echo $state_key | perl -ne 'print lc')
    mapname=us_"$statekeylc"
    filename="jquery-jvectormap-us-$statekeylc-$projection-en.js"

    #echo $state_id $state_key
    meridian=-100
    case $state_key in
        "AK") #alaska
            meridian=-130
            ;;
        "HI") #hawaii
            meridian=-150
            ;;
        "MP") #mariana islands
            meridian=146
            ;;
        "GU") #guam
            meridian=144.8
            ;;
        "PR") #puerto rico
            meridian=-66
            ;;
        "VI") # virgin islands
            meridian=-65
            ;;
        "PW") #palau
            meridian=134.5
            ;;

    esac
    echo "Generating map: $mapname > $filename"

    python ./bjornd-jvectormap-0a0575b/converter/converter.py \
           --where "STATEFP = '$state_id'" \
           --country_code_index 3 --country_name_index 3 \
           --codes_file ./USA_adm/us-counties.csv \
           --width 1000 \
           --simplify_tolerance $tolerance \
           --longitude0 $meridian \
           --projection $projection \
           --name $mapname --language en \
           ./USA_adm/tl_2013_us_county/tl_2013_us_county.shp $folder/$filename
}


for tolerance in 10000 1000 100 0
do
    for projection in mill merc aea lcc
    do
        folder=./maps/$projection-$tolerance/us
        mkdir -p $folder
        (create_us_state_map $1 $2)
    done
#wait
done
#cat USA_adm/us_states_tl.csv | xargs -n 2 ./us-counties.sh
