#!/bin/bash

country="ao"
state_key='ao-x-x'

function create_map {
    statekeylc=$(echo $state_key | perl -ne 'print lc')
    mapname=$(echo $statekeylc | sed 's/-/_/g')
    filename="jquery-jvectormap-$statekeylc-$projection-en.js"

    #echo $state_id $state_key
    meridian=16
    idx=5
    echo "Generating map: $mapname > $filename"

    python ./bjornd-jvectormap-0a0575b/converter/converter.py \
           --country_code_index $idx --country_name_index $idx \
           --codes_file ./AGO_adm/ao-counties.csv \
           --width 1000 \
           --simplify_tolerance $tolerance \
           --longitude0 $meridian \
           --projection $projection \
           --name $mapname --language en \
           ./AGO_adm/AGO_adm2.shp $folder/$filename
}

for tolerance in 10000 1000 100 0
do
    for projection in mill merc aea lcc
    do
        folder=./maps/$projection-$tolerance/
        mkdir -p $folder/

        (create_map) &
    done
    wait
done
#./ao-counties-all.sh
